package com.anhj.managingbuyerinformation.service;

import com.anhj.managingbuyerinformation.entity.LedgerCategory;
import com.anhj.managingbuyerinformation.model.CategoryListItem;
import com.anhj.managingbuyerinformation.repository.LedgerCategoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CategoryService {
    private final LedgerCategoryRepository ledgerCategoryRepository;

    public void setCategory(String categoryName) {
        LedgerCategory category = new LedgerCategory();
        category.setCategoryName(categoryName);
        category.setDateCreate(LocalDateTime.now());
        category.setDateUpdate(LocalDateTime.now());

        ledgerCategoryRepository.save(category);
    }

    public LedgerCategory getCategory(long id) {
        return ledgerCategoryRepository.findById(id).orElseThrow();
    }

    public List<CategoryListItem> getCategories() {
        List<CategoryListItem> result = new LinkedList<>();

        List<LedgerCategory> categories = ledgerCategoryRepository.findAll();

        for (LedgerCategory category : categories) {
            CategoryListItem newItem = new CategoryListItem();
            newItem.setId(category.getId());
            newItem.setCategoryName(category.getCategoryName());
            result.add(newItem);
        }
        return result;
    }

    public void putCategory(Long id, String categoryName) {
        LedgerCategory category = ledgerCategoryRepository.findById(id).orElseThrow();
        category.setId(id);
        category.setCategoryName(categoryName);
        category.setDateCreate(LocalDateTime.now());
        category.setDateUpdate(LocalDateTime.now());

        ledgerCategoryRepository.save(category);
    }

    public void delCategory(Long id) {
        ledgerCategoryRepository.findById(id);
    }
}
