package com.anhj.managingbuyerinformation.repository;

import com.anhj.managingbuyerinformation.entity.LedgerItem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LedgerItemRepository extends JpaRepository<LedgerItem, Long> {
}
