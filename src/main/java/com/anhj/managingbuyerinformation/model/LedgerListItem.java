package com.anhj.managingbuyerinformation.model;

import lombok.Getter;
import lombok.Setter;


import java.time.LocalDate;

@Getter
@Setter
public class LedgerListItem {
    private Long itemId;
    private String categoryName;
    private String itemName;
    private Double price;
    private LocalDate datePriceCriteria;
}
