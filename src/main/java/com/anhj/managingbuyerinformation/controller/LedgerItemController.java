package com.anhj.managingbuyerinformation.controller;

import com.anhj.managingbuyerinformation.entity.LedgerCategory;
import com.anhj.managingbuyerinformation.model.LedgerItemRequest;
import com.anhj.managingbuyerinformation.model.LedgerListItem;
import com.anhj.managingbuyerinformation.service.CategoryService;
import com.anhj.managingbuyerinformation.service.ItemService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/item")
public class LedgerItemController {
    private final ItemService itemService;
    private final CategoryService categoryService;

    @PostMapping("/category/{categoryId}")
    public String setItem(@PathVariable long categoryId, @RequestBody LedgerItemRequest ledgerItemRequest) {
        LedgerCategory category = categoryService.getCategory(categoryId);
        itemService.setItem(category, ledgerItemRequest);
        return "OK";
    }

    @GetMapping("/all")
    public List<LedgerListItem> getItems() {
        return itemService.getItems();
    }
}
