package com.anhj.managingbuyerinformation.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CategoryRequest {
    private String categoryName;
}
