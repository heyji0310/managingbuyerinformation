package com.anhj.managingbuyerinformation.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class LedgerItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ledgerCategoryId", nullable = false)
    private LedgerCategory ledgerCategory; // FK 관계 선언 / 항목 기준으로 N 카테고리는 1 그래서 @ManyToOne

    @Column(nullable = false, length = 20)
    private String itemName;

    @Column(nullable = false)
    private Double price;

    @Column(nullable = false)
    private LocalDate datePriceCriteria;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;
}
