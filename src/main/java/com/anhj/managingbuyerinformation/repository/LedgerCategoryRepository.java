package com.anhj.managingbuyerinformation.repository;

import com.anhj.managingbuyerinformation.entity.LedgerCategory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LedgerCategoryRepository extends JpaRepository<LedgerCategory, Long> {
}
