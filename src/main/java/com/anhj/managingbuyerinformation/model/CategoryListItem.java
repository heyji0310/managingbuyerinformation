package com.anhj.managingbuyerinformation.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CategoryListItem {
    private Long id;
    private String categoryName;
}
