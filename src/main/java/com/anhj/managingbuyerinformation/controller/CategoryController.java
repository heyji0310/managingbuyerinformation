package com.anhj.managingbuyerinformation.controller;

import com.anhj.managingbuyerinformation.model.CategoryListItem;
import com.anhj.managingbuyerinformation.model.CategoryRequest;
import com.anhj.managingbuyerinformation.service.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/category")
public class CategoryController {
    private final CategoryService categoryService;

    @PostMapping("/new")
    public String setCategory(@RequestBody CategoryRequest categoryRequest) {
        categoryService.setCategory(categoryRequest.getCategoryName());
        return "OK";
    }

    @GetMapping("/all")
    public List<CategoryListItem> getCategories() {
        return categoryService.getCategories();
    }

    @PutMapping("/{id}")
    public String putCategory(@PathVariable long id, @RequestBody CategoryRequest categoryRequest) {
        categoryService.putCategory(id, categoryRequest.getCategoryName());
        return "OK";
    }

    @DeleteMapping("/{id}")
    public String delCategory(@PathVariable long id) {
        categoryService.delCategory(id);
        return "OK";
    }
}
