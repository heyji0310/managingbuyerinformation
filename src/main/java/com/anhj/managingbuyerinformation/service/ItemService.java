package com.anhj.managingbuyerinformation.service;

import com.anhj.managingbuyerinformation.entity.LedgerCategory;
import com.anhj.managingbuyerinformation.entity.LedgerItem;
import com.anhj.managingbuyerinformation.model.LedgerItemRequest;
import com.anhj.managingbuyerinformation.model.LedgerListItem;
import com.anhj.managingbuyerinformation.repository.LedgerItemRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ItemService {
    private final LedgerItemRepository ledgerItemRepository;

    public void setItem(LedgerCategory ledgerCategory, LedgerItemRequest request) { // LedgerItemRequest는 모델에서 값을 모아서 가지고 오고, LedgerCategory는 controller에서 가지고온다.
        LedgerItem item = new LedgerItem();
        item.setLedgerCategory(ledgerCategory);
        item.setItemName(request.getItemName());
        item.setPrice(request.getPrice());
        item.setDatePriceCriteria(request.getDatePriceCriteria());
        item.setDateCreate(LocalDateTime.now());
        item.setDateUpdate(LocalDateTime.now());

        ledgerItemRepository.save(item);
    }

    public List<LedgerListItem> getItems() {
        List<LedgerListItem> result = new LinkedList<>();

        List<LedgerItem> ledgerItems = ledgerItemRepository.findAll();

        for (LedgerItem ledgerItem : ledgerItems) {
            LedgerListItem addItem = new LedgerListItem();
            addItem.setItemId(ledgerItem.getId());
            addItem.setCategoryName(ledgerItem.getLedgerCategory().getCategoryName()); // LAZY로 설정했기 떄문에 이때 가지고 온다.
            addItem.setItemName(ledgerItem.getItemName());
            addItem.setPrice(ledgerItem.getPrice());
            addItem.setDatePriceCriteria(ledgerItem.getDatePriceCriteria());

            result.add(addItem);
        }
        return result;
    }
}
